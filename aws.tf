provider "aws" {
   access_key = "AKIAJISWBHIUONP7CX4Q"
  secret_key = "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"
  region     = "us-west-1"
}

resource "aws_instance" "terec2" {
  ami           = "ami-0ec6517f6edbf8044"
  instance_type = "t2.micro"
  key_name = "task_ec2_keys_new"
  subnet_id = "${aws_subnet.public.id}"
  vpc_security_group_ids = ["${aws_security_group.sg_allow_all_tcp.id}"]

  depends_on = ["aws_internet_gateway.igw"]
  
 tags {
          Name = "Nginx"

  }
}
resource "aws_instance" "terec3" {
  ami           = "ami-0ec6517f6edbf8044"
  instance_type = "t2.micro"
    key_name = "task_ec2_keys_new"
  subnet_id = "${aws_subnet.private.id}"
    vpc_security_group_ids = ["${aws_security_group.sg_allow_all_tcp.id}"]

	depends_on = ["aws_internet_gateway.igw"]
	
 tags {
          Name = "Web Server Tomcat"

  }
}
resource "aws_instance" "terec4" {
  ami           = "ami-0ec6517f6edbf8044"
  instance_type = "t2.micro"
    key_name = "task_ec2_keys_new"
	  subnet_id = "${aws_subnet.public.id}"
    vpc_security_group_ids = ["${aws_security_group.sg_allow_all_tcp.id}"]

	depends_on = ["aws_internet_gateway.igw"]
  

 
  
 tags {
          Name = "Jenkins"

  }
  }

  resource "aws_instance" "terec5" {
  ami           = "ami-0ec6517f6edbf8044"
  instance_type = "t2.micro"
  key_name = "task_ec2_keys_new"
  subnet_id = "${aws_subnet.public.id}"
  vpc_security_group_ids = ["${aws_security_group.sg_allow_all_tcp.id}"]

  depends_on = ["aws_internet_gateway.igw"]
  
 tags {
          Name = "Ansible"

  }
}

  resource "aws_instance" "terec6" {
  ami           = "ami-0ec6517f6edbf8044"
  instance_type = "t2.micro"
  key_name = "task_ec2_keys_new"
  subnet_id = "${aws_subnet.private_build.id}"
  vpc_security_group_ids = ["${aws_security_group.sg_allow_all_tcp.id}"]

  depends_on = ["aws_internet_gateway.igw"]
  
 tags {
          Name = "Jenkins Slave"

  }
}

resource "aws_vpc" "vpc-task" {
  cidr_block       = "180.160.0.0/16"
  instance_tenancy = "default"
  enable_dns_support = true
  enable_dns_hostnames = true
  enable_classiclink = true
  enable_classiclink_dns_support = true

  tags = {
    Name = "task"
	 Location = "California"
  }
}
resource "aws_subnet" "public" {
  vpc_id     = "${aws_vpc.vpc-task.id}"
  cidr_block = "180.160.1.0/24"
  map_public_ip_on_launch = true

  depends_on = ["aws_internet_gateway.igw"]
  
  tags = {
    Name = "public"
  }
}
resource "aws_subnet" "private" {
  vpc_id     = "${aws_vpc.vpc-task.id}"
  cidr_block = "180.160.2.0/24"

  tags = {
    Name = "private"
  }
}

resource "aws_subnet" "private_build" {
  vpc_id     = "${aws_vpc.vpc-task.id}"
  cidr_block = "180.160.3.0/24"

  tags = {
    Name = "private_build"
  }
}
resource "aws_route_table" "public_rt" {
  vpc_id = "${aws_vpc.vpc-task.id}"

 route {
    cidr_block = "0.0.0.0/0"
	gateway_id = "${aws_internet_gateway.igw.id}"
  }
  
  
  tags = {
    Name = "internet route"
  }
}

resource "aws_route_table" "private_rt" {
  vpc_id = "${aws_vpc.vpc-task.id}"

 route {
    cidr_block = "0.0.0.0/0"
	gateway_id = "${aws_nat_gateway.ngt.id}"
  }
  
  
  tags = {
    Name = "natgateway route"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc-task.id}"

  tags = {
    Name = "main"
  }
}

resource "aws_route_table_association" "pr" {
  subnet_id      = "${aws_subnet.private.id}"
  route_table_id = "${aws_route_table.private_rt.id}"
}

resource "aws_route_table_association" "pr1" {
  subnet_id      = "${aws_subnet.private_build.id}"
  route_table_id = "${aws_route_table.private_rt.id}"
}

resource "aws_route_table_association" "pu" {
  subnet_id      = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.public_rt.id}"
}

resource "aws_nat_gateway" "ngt" {
  allocation_id = "${aws_eip.elb.id}"
  subnet_id     = "${aws_subnet.public.id}"
  
  depends_on = ["aws_internet_gateway.igw"]
 

  tags = {
    Name = "gw NAT"
  }
}

resource "aws_eip" "elb" {
  
  vpc      = true
  depends_on = ["aws_internet_gateway.igw"]
}

resource "aws_eip" "eip_ec2" {
vpc = true
instance = "${aws_instance.terec2.id}"

tags = {
Name = "Nginx Ip Address"
}

}

resource "aws_eip" "eip_ec2_jenkins" {
vpc = true
instance = "${aws_instance.terec4.id}"
tags = {
Name = "Jenkins Ip Address"
}

}
resource "aws_security_group" "sg_allow_all_tcp" {
  name        = "sg_allow_all_tcp"
  description = "Allow all inbound traffic"
  vpc_id = "${aws_vpc.vpc-task.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_all"
  }
  egress {
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_network_acl" "main" {
  vpc_id = "${aws_vpc.vpc-task.id}"
  subnet_ids = ["${aws_subnet.public.id}", "${aws_subnet.private.id}", "${aws_subnet.private_build.id}"]

   ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
   
   egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
   

  tags = {
    Name = "main"
  }
}

resource "aws_s3_bucket" "b" {
  bucket = "veridic-weekend-test-bucket"
}

resource "aws_s3_bucket_policy" "b" {
  bucket = "${aws_s3_bucket.b.id}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "MYBUCKETPOLICY",
  "Statement": [
    {
      "Sid": "IPAllow",
      "Effect": "Deny",
      "Principal": "*",
      "Action": [
	  "s3:Get*",
	  "s3:Put*"
	  ],
      "Resource": "arn:aws:s3:::veridic-weekend-test-bucket/*",
      "Condition": {
         "IpAddress": {"aws:SourceIp": "0.0.0.0/0"}
      }
    }
  ]
}
POLICY
}

resource "aws_route53_zone" "primary" {
  name = "vbnbharath.me"
}

resource "aws_route53_record" "www_arecord" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "vbnbharath.me"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.eip_ec2.public_ip}"]
}

resource "aws_route53_record" "www_cname" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "www"
  type    = "CNAME"
  ttl     = "5"

  records        = ["vbnbharath.me"]
}

resource "aws_route53_record" "www_cname_sub" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "jenkins01"
  type    = "CNAME"
  ttl     = "5"

  records = ["${aws_instance.terec4.public_dns}"]
}